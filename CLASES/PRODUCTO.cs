﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLASES
{
    public class PRODUCTO
    {
        #region
        public int id_producto { get; set; }
        public string nom_producto { get; set; }
        public string descripcion { get; set; }
        public int precio { get; set; }
        public int stock { get; set; }
        public int menu_id_menu { get; set; }

        public PRODUCTO(int id_producto, string nom_producto, string descripcion, int precio, int stock, int menu_id_menu)
        {
            this.id_producto = id_producto;
            this.nom_producto = nom_producto;
            this.descripcion = descripcion;
            this.precio = precio;
            this.stock = stock;
            this.menu_id_menu = menu_id_menu;
        }

        #endregion

        public bool Create() {
            try
            {

                BD.PRODUCTO prod = new BD.PRODUCTO();
                prod.ID_PRODUCTO = this.id_producto;
                prod.NOM_PRODUCTO = this.nom_producto;
                prod.DESCRIPCION = this.descripcion;
                prod.PRECIO = this.precio;
                prod.STOCK = this.precio;
                prod.MENU_ID_MENU = this.menu_id_menu;

                CommonBC.Modelo.PRODUCTO.Add(prod);
                CommonBC.Modelo.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read()
        {
            try
            {
                BD.PRODUCTO prod = new BD.PRODUCTO();
                this.id_producto = (int)prod.ID_PRODUCTO;
                this.nom_producto = prod.NOM_PRODUCTO;
                this.descripcion = prod.DESCRIPCION;
                this.precio = (int)prod.PRECIO;
                this.stock = (int)prod.STOCK;
                this.menu_id_menu = (int)prod.MENU_ID_MENU;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update()
        {
            try
            {
                BD.PRODUCTO prod = CommonBC.Modelo.PRODUCTO.First(temp => temp.ID_PRODUCTO == this.id_producto);
                prod.NOM_PRODUCTO = this.nom_producto;
                prod.DESCRIPCION = this.descripcion;
                prod.PRECIO = this.precio;
                prod.STOCK = this.stock;
                prod.MENU_ID_MENU = this.menu_id_menu;

                CommonBC.Modelo.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete()
        {
            try
            {
                BD.PRODUCTO prod = CommonBC.Modelo.PRODUCTO.First(temp => temp.ID_PRODUCTO == this.id_producto);
                CommonBC.Modelo.PRODUCTO.Remove(prod);
                CommonBC.Modelo.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
